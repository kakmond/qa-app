# JU-Q&A
JU-Q&A is the application for newcomer students that have a question about anything in Jönköping University. By simply filling out your questions, everyone on the platform will help you to answer your question, and you can answer questions that users have asked as well.

## Related Documents
For project description and visual mockups enter [here](https://gitlab.com/kakmond/qa-app/blob/master/project%20description.pdf)

## Operating Environment
This application will work on iOS mobile operating system, and internet connection of smart phone is also required for fetching data from the server.

## Use Cases
UML use-case diagram from the figure below can be described as follow:
- User can be able to see all questions, see all answers, create a new question and create an answer to a question.

![alt text](https://gitlab.com/kakmond/qa-app/raw/master/use%20case.png)