//
//  TableViewCell.swift
//  qa-app
//
//  Created by Mond on 21/11/2562 BE.
//  Copyright © 2562 qa. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var date: UILabel!
    
    // MARK: - UITableViewCell methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
