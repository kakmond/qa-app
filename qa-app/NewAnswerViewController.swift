//
//  NewAnswerViewController.swift
//  qa-app
//
//  Created by Mond on 27/11/2562 BE.
//  Copyright © 2562 qa. All rights reserved.
//

import UIKit

class NewAnswerViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var authorTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    var questionId: Int!
    
    // MARK: - Constants
    let AUTHOR_MIN_LENGTH = 3
    let AUTHOR_MAX_LENGTH = 10
    let DESCRIPTION_MIN_LENGTH = 3
    let DESCRIPTION_MAX_LENGTH = 100
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Button action listener methods
    @IBAction func newAnswer(_ sender: Any) {
        if let author = authorTextField.text, let description = descriptionTextField.text {
            var validationErrors: [String] = []
            let authorLength = author.count
            let descriptionLength = description.count
            if (authorLength < AUTHOR_MIN_LENGTH) {
                validationErrors.append("Author is too short.")
            }
            else if(authorLength > AUTHOR_MAX_LENGTH) {
                validationErrors.append("Author is too long.")
            }
            if (descriptionLength < DESCRIPTION_MIN_LENGTH) {
                validationErrors.append("Description is too short.")
            }
            else if (descriptionLength > DESCRIPTION_MAX_LENGTH) {
                validationErrors.append("Description is too long.")
            }
            if (validationErrors.count > 0) {
                // create the alert
                let alert = UIAlertController(title: "Ooops!", message: validationErrors.joined(separator: "\n"), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else {
                self.postAnswer(questionId: String(self.questionId), author: author, description: description) { (success) in
                    DispatchQueue.main.async {
                        if (success) {
                            self.navigationController?.popViewController(animated: true)
                        }
                        else {
                            // create the alert
                            let alert = UIAlertController(title: "Ooops!", message:"Server internal error", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Post data methods
    func postAnswer(questionId: String, author: String, description: String, completion: ((Bool) -> Void)?) {
        let parameters = [
            "questionId": questionId,
            "author": author,
            "description": description
        ]
        let url = URL(string: "https://q-and-a-api.herokuapp.com/answers/")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                completion?(false)
            } else {
                if let response = response as? HTTPURLResponse {
                    if (response.statusCode == 201) {
                        completion?(true)
                    }
                    else {
                        completion?(false)
                    }
                }
            }
        }
        task.resume()
    }
}
