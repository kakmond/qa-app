//
//  ExperienceDetailViewController.swift
//  qa-app
//
//  Created by Mond on 22/11/2562 BE.
//  Copyright © 2562 qa. All rights reserved.
//

import UIKit

class QuestionDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: - Properties
    @IBOutlet weak var authorQuestion: UILabel!
    @IBOutlet weak var descriptionQuestion: UILabel!
    @IBOutlet weak var dateQuestion: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var question: Question!
    var answers :[Answer] = []
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        authorQuestion.text = question.author
        descriptionQuestion.text = question.description
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // convert timestamp to date
        let unixTimeStamp: Double = Double(question.timestamp) / 1000.0
        let date = NSDate(timeIntervalSince1970: TimeInterval(unixTimeStamp))
        let dateFormatt = DateFormatter()
        dateFormatt.dateFormat = "dd/MM/yyy"
        dateQuestion.text = dateFormatt.string(from: date as Date)
        self.fetchAnswers { (success) in
            if (success){
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "answerCell", for: indexPath) as! TableViewCell
        let answer = answers[indexPath.row]
        cell.author?.text = answer.author
        cell.question?.text = answer.description
        // convert timestamp to date
        let unixTimeStamp: Double = Double(answer.timestamp) / 1000.0
        let date = NSDate(timeIntervalSince1970: TimeInterval(unixTimeStamp))
        let dateFormatt = DateFormatter()
        dateFormatt.dateFormat = "dd/MM/yyy"
        cell.date?.text = dateFormatt.string(from: date as Date)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(answers.count) Answers"
    }
    
    // MARK: - Fetch data methods
    func fetchAnswers(completion: ((Bool) -> Void)?) {
        let url = URL(string: "https://q-and-a-api.herokuapp.com/questions/\(question.id)/answers")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if (error != nil) {
                completion?(false)
            } else {
                if let data = data {
                    do {
                        self.answers = []
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        if let answers = json as? [[String: Any]]{
                            answers.forEach{ answer in
                                if let id = answer["id"] as? Int,
                                    let author = answer["author"] as? String,
                                    let createdAt = answer["createdAt"] as? Int,
                                    let description = answer["description"] as? String {
                                    let answerObject = Answer(id: id,
                                                              author: author,
                                                              description: description,
                                                              timestamp: createdAt
                                    )
                                    self.answers.append(answerObject)
                                }
                            }
                            completion?(true)
                        }
                    } catch {
                        completion?(false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Refresh methods
    @objc func refresh(_ sender: UIRefreshControl) {
        self.fetchAnswers { (success) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    // MARK: - Button action listener methods
    @IBAction func newAnswerButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "newAnswer", sender: self)
    }
    
    // MARK: - Segue methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newAnswer" {
            let controller = segue.destination as! NewAnswerViewController
            let questionId = question.id
            controller.questionId = questionId
        }
    }
    
}
