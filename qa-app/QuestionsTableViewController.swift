//
//  QuestionsTableViewController.swift
//  qa-app
//
//  Created by Mond on 21/11/2562 BE.
//  Copyright © 2562 qa. All rights reserved.
//

import UIKit

class QuestionsTableViewController: UITableViewController {
    
    // MARK: - Properties
    @IBOutlet weak var refresh: UIRefreshControl!
    var questions: [Question] = []
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refresh.beginRefreshing()
        self.fetchQuestions { (success) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refresh.endRefreshing()
            }
        }
    }
    
    // MARK: - TableView Methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath) as! TableViewCell
        let question = questions[indexPath.row]
        cell.author?.text = question.author
        cell.question?.text = question.description
        // convert timestamp to date
        let unixTimeStamp: Double = Double(question.timestamp) / 1000.0
        let date = NSDate(timeIntervalSince1970: TimeInterval(unixTimeStamp))
        let dateFormatt = DateFormatter()
        dateFormatt.dateFormat = "dd/MM/yyy"
        cell.date?.text = dateFormatt.string(from: date as Date)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Questions"
    }
    
    // MARK: - Fetch data methods
    func fetchQuestions(completion: ((Bool) -> Void)?) {
        let url = URL(string: "https://q-and-a-api.herokuapp.com/questions")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if (error != nil) {
                completion?(false)
            } else {
                if let data = data {
                    do {
                        self.questions = []
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        if let questions = json as? [[String:Any]]{
                            questions.forEach{ question in
                                if let id = question["id"] as? Int,
                                    let author = question["author"] as? String,
                                    let createdAt = question["createdAt"] as? Int,
                                    let description = question["description"] as? String {
                                    let questionObject = Question(id: id,
                                                                  author: author,
                                                                  description: description,
                                                                  timestamp: createdAt
                                    )
                                    self.questions.append(questionObject)
                                }
                            }
                             completion?(true)
                        }
                    } catch {
                         completion?(false)
                    }
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Segue methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! QuestionDetailViewController
                let question = questions[indexPath.row]
                controller.question = question
            }
        }
    }
    
    // MARK: - Refresh methods
    @IBAction func refresh(_ sender: UIRefreshControl) {
        self.fetchQuestions { (success) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refresh.endRefreshing()
            }
        }
    }
}
