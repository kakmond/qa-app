//
//  Question.swift
//  qa-app
//
//  Created by Mond on 21/11/2562 BE.
//  Copyright © 2562 qa. All rights reserved.
//

import Foundation

class Question {
    
    // MARK: - Properties
    let author: String
    let description: String
    let timestamp: Int
    let id: Int
    
    // MARK: - Initialization methods
    init(id: Int, author: String, description: String, timestamp: Int) {
        self.author = author
        self.description = description
        self.timestamp = timestamp
        self.id = id
    }
}
